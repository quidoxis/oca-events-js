# OurClubAdmin Events Calendar

This is a module to display OurClubAdmin events data, using the OurClubAdmin events API. It's currently highly dependent on:

- a server-side proxy for retrieving API data from OurClubAdmin. These endpoints are currently hardcoded to `/api/future-events.json` and `/api/past-events.json`. These endpoints must return JSON.
- Zurb Foundation 6. Some of the HTML rendered uses the F6 XY grid for display. No styling is included for this, so Foundation styles must be imported separately if not used.


## Import using NPM
Ensure your ssh keys are 
`yarn add git+https://bitbucket.org/quidoxis/oca-events-js.git`


### Javascript
Import the JS and call it. Pass the ID of the element to which the nav should stick to the bottom of:

    import OcaEvents from 'oca-events-js';
    var oca_events = new OcaEvents(options);
    window.OcaEvents = oca_events;


### Options
You can pass an optional options object to the plugin initialisation.

```
    options = {
        stickyNavTo : (string),
        eventDocsInitiallyHidden : (boolean),
        useEventTypeTags : (string)
    }

```

**stickyNavTo** _(string)_ ID of the element containing sticky nav. Creates an offset.

**eventDocsInitiallyHidden** (boolean) Choose to initially show or hide event docs.

**useEventTypeTags** (boolean) Choose to display an abbreviated event type tag before the event name


### Public methods
The plugin exposes some public methods;
```
    // Returns true if the plugin has loaded
    OcaEvents.loaded()

    // Trigger the search
    OcaEvents.search()
```

### Styles
Some basic styles are applied. Declare any colour overrides and import the base sass:
```
    $oca-text: #505050 !default;
    $oca-active: #B90000 !default;
    $oca-lightgrey: #bdbdbd !default;
    $oca-training: #004990 !default;
    $oca-racing: #B90000 !default;

    @import '../../../node_modules/oca-events-js/src/scss/events'; 
```

## Page elements
You'll need a page element which includes:

```
<section class="oca-events-calendar" id="oca-events" data-oca-rooturl="[[++oca_root_url]]" data-oca-usefilters="[[++oca_usefilters]]"></section>
```
Where
```
    [[+oca_root_url]] is the OCA subscriber URL
    [[+oca_usefilters]] is empty, or 'event_type'
```