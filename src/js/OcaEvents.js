import moment from "moment";
import * as OcaEventsUtils from "./OcaEventsUtils";
import * as Options from "./Options";


var Velocity = require("velocity-animate");
const ocaDateFormat = "YYYY-MM-DD HH:mm:ss";

const subscriberHome = OcaEventsUtils.getOcaRootUrl();

const eventsUrl = "/api/future-events.json";
const resultsUrl = "/api/past-events.json";

const token = "";

/**
 * Retrieves future events data provided by OCA through
 * the local proxy.
 *
 * @returns Promise
 */
export function getFutureEvents() {
    return new Promise(function (resolve, reject) {
        let url = eventsUrl;

        let xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.responseType = "json";
        if (token.length) {
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        }

        xhr.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                // Success!
                var resp = this.response;
                var result = processFutureEvents(resp);
                if (result.status === "success") {
                    resolve(result);
                } else {
                    reject(result.message);
                }
            } else {
                // We reached the server, but it returned an error
                reject({
                    message: "Couldn't fetch future events: " + this.status,
                    data: null,
                });
            }
        };

        xhr.onerror = function () {
            // There was a connection error of some sort
            reject({
                message: "Couldn't fetch future events",
                data: null,
            });
        };

        xhr.send();
    });
}

/**
 * Retrieves past event results data provided by OCA through
 * the local proxy.
 *
 * @returns Promise
 */
export function getPastEvents() {
    return new Promise(function (resolve, reject) {
        let url = resultsUrl;

        let xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.responseType = "json";
        if (token.length) {
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        }

        xhr.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                // Success!
                var resp = this.response;
                var result = processPastEvents(resp);
                if (result.status === "success") {
                    resolve(result);
                } else {
                    reject(result.message);
                }
            } else {
                // We reached the server, but it returned an error
                console.log("Couldn't get past events");
                reject({
                    message: "Couldn't fetch past events: " + this.status,
                    data: null,
                });
            }
        };

        xhr.onerror = function () {
            // There was a connection error of some sort
            reject({
                message: "Couldn't fetch past events, connection error",
                data: null,
            });
        };

        xhr.send();
    });
}

// Process past events. Includes some first-time-in sorting
// to reduce overhead on redraw.
let processPastEvents = function (result) {
    try {
        const today = moment();
        let eventsData;
        let eventsByYear = {};
        let eventTags = [];
        if (result.status === "success" && result.hasOwnProperty("data")) {
            eventsData = result.data.eventresults;
        } else {
            eventsData = [];
        }

        if (eventsData.length) {
            // sort the events by end date ascending (A-B)
            eventsData.sort(function compare(a, b) {
                let dateA = moment(a.end, ocaDateFormat);
                let dateB = moment(b.end, ocaDateFormat);

                return dateA.diff(dateB);
            });

            eventsData.forEach(function (ocaevent, index) {
                // if there's a future end date, apartheid.
                // don't format them yet though, they need a different sort.
                if (moment(ocaevent.end, ocaDateFormat).isSameOrAfter(today)) {
                    if (!eventsByYear.hasOwnProperty("ongoing")) {
                        eventsByYear["ongoing"] = [];
                    }
                    eventsByYear["ongoing"].push(ocaevent);
                } else {
                    let eventYear = moment(ocaevent.end, ocaDateFormat).format(
                        "YYYY"
                    );
                    if (!eventsByYear.hasOwnProperty(eventYear)) {
                        eventsByYear[eventYear] = [];
                    }
                    eventsByYear[eventYear].push(ocaevent); // add to end of array
                }

                // Process event tags
                // let tags = getEventTagsByType(ocaevent, 'sub_event_tag_1');
                // tags.forEach( (tag) => {
                //   if (! eventTags.includes(tag)) {
                //     eventTags.push(tag);
                //   }
                // });
                let tagsByType = getEventTagsByType(ocaevent);
                Object.keys(tagsByType).forEach((tagType) => {
                    if (!eventTags.hasOwnProperty(tagType)) {
                        eventTags[tagType] = tagsByType[tagType];
                    } else {
                        tagsByType[tagType].forEach((tag) => {
                            if (!eventTags[tagType].includes(tag)) {
                                eventTags[tagType].push(tag);
                            }
                        });
                    }
                });
            });

            // now, re-sort any ongoing events
            if (eventsByYear.hasOwnProperty("ongoing")) {
                if (eventsByYear["ongoing"].length > 1) {
                    eventsByYear["ongoing"].sort(function compare(a, b) {
                        let dateA = moment(a.start, ocaDateFormat);
                        let dateB = moment(b.start, ocaDateFormat);

                        return dateA.diff(dateB);
                    });
                }
            }

            return {
                status: "success",
                message: "Finished processPastEvents",
                data: eventsByYear,
                tags: eventTags,
            };
        } else {
            return {
                status: "success",
                message: "Finished processPastEvents, but none to display",
                data: null,
                tags: null,
            };
        }
    } catch (msg) {
        // console.log("BORK");
        return {
            status: "fail",
            message: "0001: Error generating past events: " + msg,
            data: null,
            tags: null,
        };
    }
};

// Process future events. Includes some first-time-in sorting
// to reduce overhead on redraw.
let processFutureEvents = function (result) {
    try {
        const today = moment();
        let eventsData;
        let eventsByYear = {};
        let eventTags = {};

        if (result.status === "success" && result.hasOwnProperty("data")) {
            eventsData = result.data.events;
        } else {
            eventsData = [];
        }

        if (eventsData.length) {
            // sort the events by end date (A-B)
            eventsData.sort(function compare(a, b) {
                let dateA = moment(a.end, ocaDateFormat);
                let dateB = moment(b.end, ocaDateFormat);

                return dateA.diff(dateB);
            });

            eventsData.forEach(function (ocaevent, index) {
                // if there's a past start date, apartheid.
                // don't format them yet though, they need a different sort.
                if (
                    moment(ocaevent.start, ocaDateFormat).isSameOrBefore(today)
                ) {
                    if (!eventsByYear.hasOwnProperty("ongoing")) {
                        eventsByYear["ongoing"] = [];
                    }
                    eventsByYear["ongoing"].push(ocaevent);
                    // console.log("Ongoing event: " + event.id);
                } else {
                    let eventYear = moment(
                        ocaevent.start,
                        ocaDateFormat
                    ).format("YYYY");
                    if (!eventsByYear.hasOwnProperty(eventYear)) {
                        eventsByYear[eventYear] = [];
                    }
                    // add to a year-based array
                    eventsByYear[eventYear].push(ocaevent);
                }

                // Process event tags
                // let tags = getEventTagsByType(ocaevent, 'event_type');
                // tags.forEach( (tag) => {
                //   if (! eventTags.includes(tag)) {
                //     eventTags.push(tag);
                //   }
                // });
                let tagsByType = getEventTagsByType(ocaevent);
                Object.keys(tagsByType).forEach((tagType) => {
                    if (!eventTags.hasOwnProperty(tagType)) {
                        eventTags[tagType] = tagsByType[tagType];
                    } else {
                        tagsByType[tagType].forEach((tag) => {
                            if (!eventTags[tagType].includes(tag)) {
                                eventTags[tagType].push(tag);
                            }
                        });
                    }
                });
            });

            // now, re-sort any ongoing events and generate some HTML
            if (eventsByYear.hasOwnProperty("ongoing")) {
                if (eventsByYear["ongoing"].length > 1) {
                    eventsByYear["ongoing"].sort(function compare(a, b) {
                        let dateA = moment(a.start, ocaDateFormat);
                        let dateB = moment(b.start, ocaDateFormat);

                        return dateA.diff(dateB);
                    });
                }
            }

            return {
                status: "success",
                message: "Finished processFutureEvents",
                data: eventsByYear,
                tags: eventTags,
            };
            // resolve("Finished processFutureEvents");
        } else {
            return {
                status: "success",
                message: "Finished processFutureEvents, but none to display",
                data: null,
                tags: null,
            };
        }
    } catch (msg) {
        // console.log("0001: Error generating future events: " + msg);
        return {
            status: "fail",
            message: "0001: Error generating future events: " + msg,
            data: null,
            tags: null,
        };
    }
};

/**
 *
 * @param {Array} pastEventsByYear
 * @param {Array} futureEventsByYear
 * @param {string} nowYear as YYYY
 * @param {string} selectedYear as YYYY
 */
export function redrawEvents(
    pastEventsByYear,
    futureEventsByYear,
    nowYear,
    selectedYear
) {
    let year = selectedYear;
    let ongoingEventIds = [];
    let thisYearEvents,
        eventsHTML = "";
    let elEvents = document.querySelector("#oca-eventsdata");
    // display that year by replacing events contents
    if (year == nowYear) {
        // revert to initial state?
        eventsHTML += '<div id="oca-pastevents">';
        if (pastEventsByYear && pastEventsByYear.hasOwnProperty(year)) {
            thisYearEvents = pastEventsByYear[year];

            thisYearEvents.forEach(function (event, index) {
                let eventHTML = generatePastEvent(event);
                eventsHTML += eventHTML;
            });
        } else {
            eventsHTML += "<p>There have been no previous events this year</p>";
        }
        eventsHTML += "</div>"; // close #pastevents

        // today separator
        eventsHTML += '<hr id="oca-today">';

        eventsHTML += '<div id="oca-ongoingevents">';
        if (pastEventsByYear && pastEventsByYear.hasOwnProperty("ongoing")) {
            thisYearEvents = pastEventsByYear["ongoing"];

            if (thisYearEvents.length > 1) {
                // re-sort them by start date asc
                thisYearEvents.sort(function compare(a, b) {
                    let dateA = moment(a.start, ocaDateFormat);
                    let dateB = moment(b.start, ocaDateFormat);

                    return dateA.diff(dateB);
                });
            }

            thisYearEvents.forEach(function (event, index) {
                let eventHTML = generatePastEvent(event); // render it in the pastevent style
                eventsHTML += eventHTML;
                // add to list of ongoing event IDs, so we don't re-draw in future
                ongoingEventIds.push(event.id);
            });
        }

        if (
            futureEventsByYear &&
            futureEventsByYear.hasOwnProperty("ongoing")
        ) {
            thisYearEvents = futureEventsByYear["ongoing"];

            if (thisYearEvents.length > 1) {
                // re-sort them by start date asc
                thisYearEvents.sort(function compare(a, b) {
                    let dateA = moment(a.start, ocaDateFormat);
                    let dateB = moment(b.start, ocaDateFormat);

                    return dateA.diff(dateB);
                });
            }

            thisYearEvents.forEach(function (event, index) {
                // If we haven't shown it already
                if ( ongoingEventIds.includes(event.id) == false ) {
                    let eventHTML = generateFutureEvent(event); // render it in the pastevent style
                    eventsHTML += eventHTML;
                }
            });
        }
        eventsHTML += "</div>"; // close #ongoingevents

        eventsHTML += '<div id="oca-futureevents">';

        if (futureEventsByYear && futureEventsByYear.hasOwnProperty(year)) {
            thisYearEvents = futureEventsByYear[year];
            thisYearEvents.forEach(function (event, index) {
                let eventHTML = generateFutureEvent(event);
                eventsHTML += eventHTML;
            });
        } else {
            eventsHTML +=
                "<p>There are currently no future events scheduled this year</p>";
        }
        eventsHTML += "</div>"; // close #futureevents
    } else if (year > nowYear) {
        // future events
        // console.log("Events for: " + year);
        thisYearEvents = futureEventsByYear[year];
        thisYearEvents.forEach(function (event, index) {
            let eventHTML = generateFutureEvent(event);
            eventsHTML += eventHTML;
        });
    } else if (year < nowYear) {
        // past events
        // console.log("Event results for: " + year);
        thisYearEvents = pastEventsByYear[year];
        thisYearEvents.forEach(function (event, index) {
            let eventHTML = generatePastEvent(event);
            eventsHTML += eventHTML;
        });
    } else {
        console.log(
            "Undefined year selected: " + year + " against now: " + nowYear
        );
    }

    // spit the HTML out
    elEvents.innerHTML = eventsHTML;

    if (year == nowYear) {
        scrollToToday();
    } else {
        scrollToFirst();
    }
}

/**
 * Generates HTML for a future event
 *
 * @param {Object} ocaevent
 * @returns {string}
 */
export function generateFutureEvent(ocaevent) {
    let options = Options.getOptions();
    let startDate = moment(ocaevent.start, ocaDateFormat);
    let endDate = moment(ocaevent.end, ocaDateFormat);
    let displayDates = generateDisplayDates(startDate, endDate);
    let status = ocaevent.status;
    let statusHTML = "";

    if (status == "open") {
        statusHTML = `<a href="${subscriberHome}/enter/${ocaevent.id}" class="small button" target="_blank">Enter</a>`;
    } else {
        statusHTML = "&nbsp;";
    }

    let eventTypes = getEventTagsForType(ocaevent, "event_type");
    let eventType_label = "";
    let eventType_class = "";

    if (eventTypes.length) {
        eventType_class = generateEventTypeClass(eventTypes[0]);
        eventType_label = generateEventTypeLabel(eventTypes);
    }

    // if (ocaevent.siteURL) {
    //     statusHTML = `<a href="${siteURL}" class="small button" target="_blank">Enter</a>`;
    // }

    let venue = ocaevent.venue.name;
    let minieventURL = ocaevent.pageURL;
    let html = 
      `<div id="oca-event${ocaevent.id}" data-event="${ocaevent.id}" data-event-monthstart="${startDate.month()}" data-event-monthend="${endDate.month()}" class="grid-x grid-padding-x grid-padding-y event ${eventType_class}">
        <div class="cell small-4 medium-2">${displayDates}</div>
        <div class="cell small-8 medium-8">${options.useEventTypeTags ? eventType_label : ``} <a class="eventMiniURL newWindowNoIcon" href="${minieventURL}">${ocaevent.name}</a> <span class="event-venue">${venue}</span> <a href="${minieventURL}" class="event-more newWindowNoIcon">More info</a></div>
        <div class="cell medium-2 text-right">${statusHTML}</div>
      </div>`;
    return html;
}

/**
 * Generates HTML for a past event
 *
 * @param {Object} ocaevent
 * @returns {string}
 */
export function generatePastEvent(ocapastevent) {
    let startDate = moment(ocapastevent.start, ocaDateFormat);
    let endDate = moment(ocapastevent.end, ocaDateFormat);
    let displayDates = generateDisplayDates(startDate, endDate);

    let venue = ocapastevent.venue.name;
    let minieventURL = ocapastevent.pageURL;

    let documents = generateDocuments(ocapastevent.documents, "resultdocs");

    let options = Options.getOptions();
    let chevron = "fa-chevron-up";
    if ( options.eventDocsInitiallyHidden ) {
        chevron = "fa-chevron-down";
    }


    let html = 
    `<div id="oca-event${ocapastevent.id}" data-event="${ocapastevent.id}" data-event-monthstart="${startDate.month()}" data-event-monthend="${endDate.month()}" class="grid-x grid-padding-x grid-padding-y event">
      <div class="cell small-4 medium-2">${displayDates}</div>
      <div class="cell small-8 medium-5"><a class="eventMiniURL newWindowNoIcon" href="${minieventURL}">${ocapastevent.name}</a> <span class="event-venue">${venue}</span> <a href="${minieventURL}" class="event-more newWindowNoIcon show-for-medium">More info</a></div>
      <div class="cell small-12 medium-5 oca-eventdocs">`;
    
    if (documents.length) {
        html += 
        `<div class="oca-eventdocs--title">Event results <i class="fas ${chevron}"></i></div>
          <div class="oca-eventdocs--docs" ${options.eventDocsInitiallyHidden ? `style="display:none"` : ``}>${documents}</div>`;
    }
      html += 
      `</div>
      <div class="cell small-12 show-for-small-only text-right"><a href="${minieventURL}" class="event-more newWindowNoIcon">More info</a></div>
    </div>`;  
    return html;
}

/**
 * Generates html for the event to/from dates
 *
 * @param {moment} startDate
 * @param {moment} endDate
 * @returns {string}
 */
function generateDisplayDates(startDate, endDate) {
    let displayDates;
    try {
        if (startDate.isSame(endDate, "day")) {
            displayDates = htmlifyDate(startDate);
        } else {
            displayDates =
                '<div class="date-range">' +
                htmlifyDate(startDate) +
                '<span class="date-to">-</span>' +
                htmlifyDate(endDate) +
                "</div>";
        }
        return displayDates;
    } catch (err) {
        displayDates = "";
        console.log("Error generating display dates: " + err);
    }
}

/**
 * Generates html for a single to/from date
 *
 * @param {Date} date
 */
function htmlifyDate(date) {
    let html = `<div class="event-date"><div class="event-date--month">${date.format(
        "MMM"
    )}<span class="event-date--year">${date.format(
        "YYYY"
    )}</span></div><div class="event-date--day">${date.format(
        "DD"
    )}</div></div>`;
    return html;
}

export function generateDocuments(documents, listclass) {
    let html = "";
    if (Object.keys(documents).length) {
        html = `<ul class="${listclass}">`;

        for (let docId in documents) {
            if (!documents.hasOwnProperty(docId)) {
                continue;
            }
            html += generateDocument(documents[docId]);
        }
        html += "</ul>";
    } else {
        console.log("No documents");
    }
    return html;
}

export function generateDocument(document) {
    let html = `<li class="resultdoc"><a class="newWindowNoIcon" href="${document.url}" title="${document.description}"><i class="fas fa-medal"></i> <span>${document.title}</span></a></li>`;
    return html;
}

export function scrollToToday() {
    const today = document.getElementById("oca-today");

    scrollToElement(today);
}

export function scrollToElement(element) {
    if (element) {
        let siteNavHeight = 0;

        const eventsNav = document.getElementById("oca-events-nav");
        const eventsNavHeight = parseFloat(
            getComputedStyle(eventsNav, null).height.replace("px", "")
        );
        // or maybe just
        // const eventsNavHeight = eventsNav.offsetHeight;

        const siteNav = document.getElementById("nav");
        if (siteNav) {
            siteNavHeight = parseFloat(
                getComputedStyle(siteNav, null).height.replace("px", "")
            );
        }

        let scrollOffset = siteNavHeight + eventsNavHeight + 30;
        // console.log(navOffset);

        Velocity(element, "scroll", { duration: 750, offset: -scrollOffset });
    }
}

export function scrollToFirst() {
    const eventsData = document.getElementById("oca-eventsdata");
    if (eventsData) {
        console.log("Scrolling to first event");
        // let navOffset = $("#event-years").offset().top - ($(window).scrollTop() - $("#event-years").height() - 30);

        let siteNavHeight = 0;
        const eventsNav = document.getElementById("oca-events-nav");
        const eventsNavHeight = parseFloat(
            getComputedStyle(eventsNav, null).height.replace("px", "")
        );

        const siteNav = document.getElementById("nav");
        if (siteNav) {
            siteNavHeight = parseFloat(
                getComputedStyle(siteNav, null).height.replace("px", "")
            );
        }

        let scrollOffset = siteNavHeight + eventsNavHeight + 30;

        Velocity(eventsData, "scroll", {
            duration: 750,
            offset: -scrollOffset,
        });
    }
}

function generateYearNavElement(year) {
    let yearElement = document.createElement("li");
    yearElement.dataset.year = year;
    yearElement.innerHTML = `<button class="select-event-year" data-year="${year}" href="#" aria-label="${year}">${year}</button>`;
    return yearElement;
}

function generateYearNavOption(year) {
    let yearOption = document.createElement("option");
    yearOption.value = year;
    yearOption.dataset.year = year;
    yearOption.text = year;
    return yearOption;
}

function generateActiveYearNavElement(year) {
    let yearHTML = `<li class="current" data-year="${year}"><span class="show-for-sr">You're at</span>${year}</li>`;
    return yearHTML;
}

export function swapNavElement(el) {
    deactivateActiveNavElements();

    // clicked element parent li
    let clickedLI = el.parentElement;

    // Highlight selected, if it has a year
    if (clickedLI) {
        const replacementHTML = generateActiveYearNavElement(el.dataset.year);
        // insert new, remove old
        clickedLI.insertAdjacentHTML("afterend", replacementHTML);
        clickedLI.parentNode.removeChild(clickedLI);
    }
}

export function deactivateActiveNavElements() {
    // current active
    let currentLI = document.querySelector("#oca-event-years li.current");

    if (currentLI) {
        let currentlyHighlightedYear = currentLI.dataset.year;
        // replace currently highlighted with a regular nav button
        const elReplacement = generateYearNavElement(currentlyHighlightedYear);
        currentLI.insertAdjacentElement("afterend", elReplacement);
        currentLI.parentNode.removeChild(currentLI);
    }
}

export function buildYearNav(calendarRoot, pastEvents, futureEvents, nowYear) {
    let eventYearNavBuilt = new Event("event-nav-built");
    let pastEventsByYear, futureEventsByYear;
    let eventYearNav = document.getElementById("oca-event-years");
    let eventYearNavMobile = document.getElementById("oca-event-years-mobile");

    // Build year-based navigation
    if (pastEvents.data !== null) {
        pastEventsByYear = pastEvents.data;
        let pastYears = Object.keys(pastEventsByYear);
        pastYears.sort(function compare(a, b) {
            return b - a;
        });
        pastYears.forEach((year) => {
            if (nowYear !== year && year !== "ongoing") {
                let dEl = generateYearNavElement(year);
                eventYearNav.insertBefore(dEl, eventYearNav.firstChild);
                let mEl = generateYearNavOption(year);
                eventYearNavMobile.insertBefore(
                    mEl,
                    eventYearNavMobile.firstChild
                );
            }
        });
        // console.log(pastYears);
    } else {
        console.log(pastEvents.message);
    }

    if (futureEvents.data !== null) {
        futureEventsByYear = futureEvents.data;
        let futureYears = Object.keys(futureEventsByYear);
        futureYears.sort(function compare(a, b) {
            return a - b;
        });
        futureYears.forEach((year) => {
            if (nowYear !== year && year !== "ongoing") {
                let dEl = generateYearNavElement(year);
                eventYearNav.appendChild(dEl);
                let mEl = generateYearNavOption(year);
                eventYearNavMobile.appendChild(mEl);
            }
        });
        // console.log(futureYears);
    } else {
        console.log(futureEvents.message);
    }
    calendarRoot.dispatchEvent(eventYearNavBuilt);
}

/**
 *
 * @param {Object} eventsByYear
 * @param {string} regex
 * @param {string} searchfilter
 * @returns {Array}
 */
function searchEventsByYear(eventsByYear, regex, searchfilter) {
    var years = Object.keys(eventsByYear);
    let res = [];
    years.forEach((year) => {
        var yeardata = eventsByYear[year]; // array of event objects
        var matchedevent = yeardata.filter(function (event) {
            var match_name = eventNameMatches(event.name, regex);
            var match_venue = eventNameMatches(event.venue.name, regex);

            // if a filter is specified, be strict about using it
            if (searchfilter) {
                var match_tag = eventTagsMatches(event, searchfilter);
                return (match_name || match_venue) && match_tag;
            } else {
                var match_tag = eventTagsMatches(event, regex);
                return match_name || match_venue || match_tag;
            }
        });
        res = res.concat(matchedevent);
    });
    return res;
}

function eventNameMatches(eventname, regex) {
    if (eventname.length > 1) {
        return eventname.search(regex) !== -1;
    } else {
        return false;
    }
}

function eventTagsMatches(event, regex) {
    var found = false;
    var tagIds = Object.keys(event.tags);
    if (tagIds.length) {
        tagIds.forEach((tagId) => {
            // console.log(eventtags[tagId].text);
            var hasTagMatch = event.tags[tagId].text.search(regex) !== -1;
            // if (hasTagMatch) {
            //     console.log("Tag matched for eventId " + event.id);
            // }
            found = found || hasTagMatch;
        });
    }
    return found;
}

export function searchEvents(
    pastEventsByYear,
    futureEventsByYear,
    searchstring,
    searchfilter
) {
    // console.log("Search for " + searchstring);
    var regex = new RegExp(searchstring, "i");

    var futureMatches = searchEventsByYear(
        futureEventsByYear,
        regex,
        searchfilter
    );
    var pastMatches = searchEventsByYear(pastEventsByYear, regex, searchfilter);
    var total = futureMatches.length + pastMatches.length;

    return {
        count: total,
        pastMatches: pastMatches,
        futureMatches: futureMatches,
    };
}

export function renderSearchResults(results) {
    let futureMatches = results.futureMatches;
    let pastMatches = results.pastMatches;
    let pastResultsHTML = "";
    let futureResultsHTML = "";
    let today = '<hr id="oca-today">';

    if (pastMatches.length) {
        pastMatches.forEach(function (event, idx) {
            let eventHTML = generatePastEvent(event);
            pastResultsHTML += eventHTML;
        });
    }
    if (futureMatches.length) {
        futureMatches.forEach(function (event, idx) {
            let eventHTML = generateFutureEvent(event);
            futureResultsHTML += eventHTML;
        });
    }

    if (futureResultsHTML !== "" && pastResultsHTML !== "") {
        return pastResultsHTML + today + futureResultsHTML;
    } else {
        return pastResultsHTML + futureResultsHTML;
    }
}

/**
 * Resets the events search input
 */
export function resetSearch() {
    const qs = document.getElementById("oca-qs");
    const tagFilter = document.getElementById("oca-qs-filter");
    qs.value = "";
    if (tagFilter) {
        tagFilter.value = "";
        tagFilter.selectedIndex = 0;
    }
}

export function resetMonthsFilter() {
    const ocaevents = document.querySelectorAll("#oca-eventsdata .event");
    ocaevents.forEach(function (ocaevent, idx) {
        ocaevent.style.display = "flex";
    });
}

export function filterForMonth(month) {
    if (month.classList.contains("month")) {
        let selectedMonthVal = month.dataset.month - 1;
        // filter the current display by month by hiding other months
        let ocaevents = document.querySelectorAll("#oca-eventsdata .event");
        if (ocaevents.length) {
            ocaevents.forEach(function (ocaevent, idx) {
                let monthstart = parseInt(ocaevent.dataset.eventMonthend);
                let monthend = parseInt(ocaevent.dataset.eventMonthend);

                if (
                    (monthstart !== selectedMonthVal &&
                        monthend !== selectedMonthVal) ||
                    (monthstart > selectedMonthVal &&
                        monthend < selectedMonthVal)
                ) {
                    ocaevent.style.display = "none";
                } else {
                    ocaevent.style.display = "flex";
                }
            });
        }
    } else {
        // Assume the 'all' button has been clicked.
        // Un-hide all events.
        resetMonthsFilter();
    }
}

export function buildTagsFilter(tags) {
    let elSelect = null;
    if (tags !== null && tags.length) {
        elSelect = document.createElement("select");
        elSelect.name = "oca-qs-filter";
        elSelect.id = "oca-qs-filter";
        elSelect.className = "input-group-field";

        // Build an initial, value-less option
        let elInitOption = document.createElement("option");
        elInitOption.text = "- any -";
        elInitOption.value = "";
        elSelect.appendChild(elInitOption);

        // Add the tags as options
        tags.forEach((tag) => {
            let elOption = document.createElement("option");
            elOption.value = elOption.text = tag;

            elSelect.appendChild(elOption);
        });
    }

    return elSelect;
}

function generateEventTypeLabel(eventTypes) {
    let html = "";

    if (eventTypes.length) {
        eventTypes.forEach((et) => {
            let words = et.split(" ");
            let label = "";

            if (words.length > 1) {
                // if many words, first letters from each as caps
                words.forEach(function (word) {
                    label += word.substr(0, 1).toUpperCase();
                });
            } else {
                // else, first two letters
                label = et.substring(0, 2);
            }
            html += `<span class="eventtype-label">${label}</span>`;
        });
    }

    return html;
}

// generates a string suitable for use as a className
function generateEventTypeClass(eventType) {
    return eventType.replace(/\s+/g, "").toLowerCase();
}

/**
 * Builds an array of event tag strings for a given OCA tag type
 * e.g. ['Training', 'Racing']
 *
 * @param {Object} ocaevent
 * @param {string} type e.g. event_type, sub_event_tag_1
 * @returns {Array} tags
 *
 */
function getEventTagsForType(ocaevent, type) {
    let tagIds = Object.keys(ocaevent.tags);
    let tags = [];
    if (tagIds.length) {
        tagIds.forEach((tagId) => {
            if (ocaevent.tags[tagId].systemtype === type) {
                tags.push(ocaevent.tags[tagId].text);
            }
        });
    }
    return tags;
}

/**
 *
 * @param {Object} ocaevent
 * @returns {Object} An object of tags by type for the given event
 */
function getEventTagsByType(ocaevent) {
    let tagsByType = {};
    let tagIds = Object.keys(ocaevent.tags);
    if (tagIds.length) {
        tagIds.forEach((tagId) => {
            let tagType = ocaevent.tags[tagId].systemtype;
            if (!tagsByType.hasOwnProperty(tagType)) {
                tagsByType[tagType] = [];
                tagsByType[tagType] = getEventTagsForType(ocaevent, tagType);
            }
        });
    }
    return tagsByType;
}

export function doSearch(pastEventsByYear, futureEventsByYear) {
    let message = "";
    let qs = document.getElementById("oca-qs").value;
    const filter = document.getElementById("oca-qs-filter");
    let filterVal = null;
    const eventsData = document.getElementById("oca-eventsdata");
    let searchResults = document.createElement("div");
    searchResults.setAttribute("id", "oca-searchresults");
    deactivateActiveNavElements(); // disable current active nav
    // empty eventsData
    while (eventsData.lastChild) {
        eventsData.removeChild(eventsData.lastChild);
    }

    if (filter) {
        filterVal = filter.value;
    }
    if (qs.length >= 3 || ((qs.length == 0 || qs.length >= 3) && filterVal)) {
        qs = qs.trim();
        let results = searchEvents(
            pastEventsByYear,
            futureEventsByYear,
            qs,
            filterVal
        );
        if (results.count > 0) {
            message = "<p><strong>Filtering events ";
            if (qs.length) {
                message += "for <i>" + qs + "</i> ";
            }
            if (filterVal) {
                message += "with tag " + "<i>" + filterVal + "</i>";
            }
            message += ".</strong> Total found: " + results.count + "</p>";
            message += renderSearchResults(results);
        } else {
            message = "<p><strong>No events found ";
            if (qs.length) {
                message += "containing <i>" + qs + "</i> ";
            }
            if (filterVal) {
                message += "with tag " + "<i>" + filterVal + "</i>";
            }
            message += ".</strong></p>";
        }
    } else {
        message =
            "<p><strong>Enter more than three characters to search</strong></p>";
    }
    searchResults.innerHTML = message;
    eventsData.appendChild(searchResults);
    if (document.getElementById("oca-today")) {
        scrollToToday();
    } else {
        scrollToElement(eventsData);
    }
}
