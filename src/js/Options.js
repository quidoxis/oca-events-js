const defaultOptions = {
    stickyNavTo: null,
    eventDocsInitiallyHidden: false,
    useEventTypeTags: true
};

let currentOptions = defaultOptions;

export function getOptions() {
    
    return currentOptions;
}

export function setOptions(userOptions) {

    currentOptions = Object.assign({}, defaultOptions, userOptions);

    return currentOptions;    
}
