/**
 *
 * @returns string Root OCA datasource url
 */
export function getOcaRootUrl() {
  const ocaEvents = document.getElementById("oca-events");
  if (ocaEvents && 'ocaRooturl' in ocaEvents.dataset ) {
    return ocaEvents.dataset.ocaRooturl;
  } else {
    return null;
  }
}

/**
 * 
 * @returns boolean 
 */
export function useSearchFilters() {
  const ocaEvents = document.getElementById("oca-events");
  if (ocaEvents && "ocaUsefilters" in ocaEvents.dataset) {
    if ( ocaEvents.dataset.ocaUsefilters.length ) {
      return ocaEvents.dataset.ocaUsefilters;
    } else {
      return false;
    }
  } else {
      return false;
  }
}



