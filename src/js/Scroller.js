let Velocity = require('velocity-animate');

export function init(elMenu, elItem){


    // duration of scroll animation
    var scrollDuration = 300;
    // paddles
    var menuWrapper = elMenu.parentNode;
    var leftPaddle = menuWrapper.getElementsByClassName('left-paddle')[0];
    var rightPaddle = menuWrapper.getElementsByClassName('right-paddle')[0];

    var items = elMenu.querySelectorAll(elItem);
    // get some relevant size for the paddle triggering point
    var paddleMargin = 20;

    // get wrapper width
    var getMenuWrapperSize = function() {
        return menuWrapper.offsetWidth;
    }
    var menuWrapperSize = getMenuWrapperSize();
    // size of the visible part of the menu is equal as the wrapper size 
    var menuVisibleSize = menuWrapperSize;

    // get total width of all menu items
    var getMenuSize = function() {
        var itemsLength = 0;
        [].forEach.call(items, (item) => {
            itemsLength += item.offsetWidth;
        });
        return itemsLength;
    };
    var menuSize = getMenuSize();
    // get how much of menu is invisible
    var menuInvisibleSize = menuSize - menuWrapperSize;

    // get how much have we scrolled to the left
    var getMenuPosition = function() {
        return elMenu.scrollLeft;
    };

    // finally, what happens when we are actually scrolling the menu
    // $('.menu').on('scroll', function() {
    elMenu.onscroll = function(){

        // get how much of menu is invisible
        menuInvisibleSize = menuSize - menuWrapperSize;
        // get how much have we scrolled so far
        var menuPosition = getMenuPosition();

        var menuEndOffset = menuInvisibleSize - paddleMargin;

        // show & hide the paddles 
        // depending on scroll position
        if (menuPosition <= paddleMargin) {
            leftPaddle.classList.add('hide');
            rightPaddle.classList.remove('hide');
        } else if (menuPosition < menuEndOffset) {
            // show both paddles in the middle
            leftPaddle.classList.remove('hide');
            rightPaddle.classList.remove('hide');
        } else if (menuPosition >= menuEndOffset) {
            leftPaddle.classList.remove('hide');
            rightPaddle.classList.add('hide');
        }

    };

    // scroll to left
    rightPaddle.addEventListener("click", function() {
        let scrollDistance = menuWrapperSize / 2;
        Velocity(elMenu, 'scroll', { axis: "x", container: elMenu, offset: scrollDistance });
    });    

    // scroll to right
    leftPaddle.addEventListener("click", function() {
        let scrollDistance = menuWrapperSize / 2;
        Velocity(elMenu, 'scroll', { axis: "x", container: elMenu, offset: -scrollDistance });
    });       


    // the wrapper is responsive
    window.addEventListener('resize', function() {
        menuWrapperSize = getMenuWrapperSize();
    });

    console.log("Setting initial scroll to ",menuSize);
    elMenu.scrollLeft = menuSize;

}