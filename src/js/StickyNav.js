import { throttle, debounce } from 'throttle-debounce';

export default function(eventOptions) {
  // Sticky event nav
  if (eventOptions.stickyNavTo) {
    const elStickyNavTo = document.getElementById(eventOptions.stickyNavTo);
    if (elStickyNavTo) {
      console.info("Using sticky events nav");
      stickyEventsNav(elStickyNavTo);
    }
  }
}

function setStickyTop(ocaEventsNav, elStickyBelow) {
  let headerHeight = elStickyBelow.offsetHeight;
  ocaEventsNav.style.top = headerHeight + "px";
}

function stickyEventsNav(elStickyNavTo) {
  let ocaEventsNav = document.getElementById("oca-events-nav");

  if (ocaEventsNav) {
    // set the initial offset
    setStickyTop(ocaEventsNav, elStickyNavTo);

    window.addEventListener(
      "resize",
      debounce(500, function (e) {
        setStickyTop(ocaEventsNav, elStickyNavTo);
      })
    );
    window.addEventListener(
      "scroll",
      throttle(300, function (e) {
        setStickyTop(ocaEventsNav, elStickyNavTo);
      })
    );
  }
}
