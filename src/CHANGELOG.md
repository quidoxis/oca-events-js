# Changelog

## [1.5.10] - 2024-11-03
### Fixed
- Don't display an ongoing event twice

### [1.5.8] - 2022-07-26
### Added
- useEventTypeTags option
### Fixed
- missing fontawesome medal icon

## [1.5.6] - 2022-06-08
### Added
- Emit event of "ocaEventsReady" on root element when ready

## [1.5.5] - 2022-06-06
### Fixed
- Correctly filter unique tags for events search

## [1.5.2] - 2022-05-19
### Added
- Show/hide event result documents
- Option to initially show/hide event result docs

## [1.5.1] - 2022-05-18
### Changed
- Trigger event on OCA calendar root element for easier listening. Smooth, man.

## [1.5.0] - 2022-05-18
### Added
- Some public methods: search, loaded, state

## [1.4.2] - 2022-05-17
### Changed
- Ensure requested filter type exists

## [1.4.1] - 2022-05-17
### Changed
- Correctly identify use of filters

## [1.4.0] - 2022-05-17
### Added
- Display filters according to config, e.g. (event_type, sub_event_tag1 etc)

## [1.3.0] - 2022-04-19
### Added
- Make sticky events nav configurable

## [1.2.1] - 2022-04-19
### Fixed
- Find the OCA root URL in the right location

## [1.2.0] - 2022-04-19
### Changed
- Remove Foundation sticky, use CSS3 property.
- Remove depedency on F6 sass functions

## [1.1.0] - 2022-04-19
### Changed
- Removed jQuery dependency, using the non-jQuery syntax for Velocity animation library (used in year scroller)

## [1.0.0] - 2022-04-14
### Initial release
- First release of npm package for the OurClubAdmin Events JS module. 