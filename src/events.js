/**
 * Retrieves OurClubAdmin events and results data from a proxy endpoint provided by
 * the Wordpress REST API, which in turn retrieves data directly from the subscriber's
 * OurClubAdmin REST API endpoint.
 *
 * @author Andrew Wallace
 * @copyright Quidoxis Ltd 2022
 *
 */
 import moment from "moment";
 import * as OcaEvents from "./js/OcaEvents";
 import * as Scroller from "./js/Scroller";
 import StickyNav from "./js/StickyNav";
 import * as OcaEventsUtils from "./js/OcaEventsUtils";
 import * as Options from "./js/Options";
 
 import { library, dom } from "@fortawesome/fontawesome-svg-core";
 import {
     faSearch,
     faCircleNotch,
     faFileDownload,
     faArrowRight,
     faArrowLeft,
     faChevronLeft,
     faChevronRight,
     faChevronDown,
     faChevronUp,
     faMedal
 } from "@fortawesome/free-solid-svg-icons";

 import { 
     faDotCircle    
} from "@fortawesome/free-regular-svg-icons";

 library.add(
     faSearch,
     faCircleNotch,
     faDotCircle,
     faFileDownload,
     faArrowRight,
     faArrowLeft,
     faChevronLeft,
     faChevronRight,
     faChevronDown,
     faChevronUp,
     faMedal
 );
 
 export default function events(options) {
    //  const option_defaults = {
    //      stickyNavTo: null,
    //      eventDocsInitiallyHidden: false
    //  };
 
    //  const eventOptions = Object.assign({}, option_defaults, options);
    // Options.setOptions(options);
    const eventOptions = Options.setOptions(options);
    const pluginReady = new Event('ocaEventsReady');
 
     let state = "uninitialised";
     let pastEventsByYear = {},
         futureEventsByYear = {};
 
     dom.watch(); // Fontawesome
     document.addEventListener("DOMContentLoaded", function () {
         const calendarRoot = document.getElementById("oca-events");
 
         if (calendarRoot) {
             let nowYear = moment().format("YYYY");
 
             renderSkeleton(calendarRoot, nowYear);
 
             let elsGotoToday = document.getElementsByClassName("goToToday");
 
             const eventYearsMobile = document.getElementById(
                 "oca-event-years-mobile"
             );
             const eventYearsNav = document.getElementById("oca-event-years");
 
             Promise.all([
                 OcaEvents.getFutureEvents(),
                 OcaEvents.getPastEvents(),
             ])
                 .then(([futureEvents, pastEvents]) => {
                     console.info("OCA calendar data retrieved");
                     // console.log({"Future": futureEvents});
                     // console.log({"Past" : pastEvents});
                     pastEventsByYear = pastEvents.data;
                     futureEventsByYear = futureEvents.data;
 
                     // Render!
                     OcaEvents.redrawEvents(
                         pastEventsByYear,
                         futureEventsByYear,
                         nowYear,
                         nowYear
                     );
                     let waiting = document.querySelector(
                         "#oca-eventsdata .waiting"
                     );
                     if (waiting) {
                         waiting.parentNode.removeChild(waiting);
                     }
 
                     // Build the search filter with available tags
                     if (OcaEventsUtils.useSearchFilters() !== false) {
                         let tagsFilter = OcaEvents.buildTagsFilter(
                            getUniqueTagsOfType(
                                 pastEvents,
                                 futureEvents,
                                 OcaEventsUtils.useSearchFilters()
                             )
                         );
                         if (tagsFilter) {
                             document
                                 .getElementById("oca-qs")
                                 .insertAdjacentElement("afterend", tagsFilter);
                         } else {
                             console.log("No tags for filter");
                         }
                     }
 
                     OcaEvents.buildYearNav(calendarRoot, pastEvents, futureEvents, nowYear);
                     OcaEvents.scrollToToday();
                     state = "loaded";
                     calendarRoot.dispatchEvent(pluginReady);
                 })
                 .catch(function (err) {
                     let eventsData = document.getElementById("oca-eventsdata");
                     eventsData.innerHTML =
                         "<div class='text-center'><p>There's been an error displaying events :(</p><p>&nbsp;</p><p>&nbsp;</p></div>";
                     let waiting = document.querySelector(
                         "#oca-eventsdata .waiting"
                     );
                     if (waiting) {
                         waiting.parentNode.removeChild(waiting);
                     }
                     state = 'error';
                     console.log("Something went wrong. " + err);
                 });
 
             // Re-draw events on nav click
 
             if (eventYearsNav) {
                 eventYearsNav.addEventListener("click", (e) => {
                     if (e.target.classList.contains("select-event-year")) {
                         console.log("Year select");
                         let eventYear = e.target;
                         e.preventDefault;
                         OcaEvents.resetSearch();
                         let selectedYear = eventYear.dataset.year;
                         OcaEvents.redrawEvents(
                             pastEventsByYear,
                             futureEventsByYear,
                             nowYear,
                             selectedYear
                         );
                         // swap out nav
                         OcaEvents.swapNavElement(eventYear);
                     }
                 });
             }
 
             if (eventYearsMobile) {
                 eventYearsMobile.addEventListener("change", (e) => {
                     let selectedYear = e.target.value;
                     OcaEvents.resetSearch();
                     OcaEvents.redrawEvents(
                         pastEventsByYear,
                         futureEventsByYear,
                         nowYear,
                         selectedYear
                     );
                 });
             }
 
             // Events search
             const eventSearchForm = document.getElementById("oca-event-search");
             if (eventSearchForm) {
                 eventSearchForm.addEventListener("submit", (e) => {
                     e.preventDefault();
                     search();
                 });
             }
 
             calendarRoot.addEventListener("event-nav-built", (e) => {
                 console.info("Event nav built");
                 Scroller.init(document.getElementById("oca-event-years"), "li");
                 StickyNav(eventOptions);
             });
 
             /**
              * Click handler for the month selector
              */
             const months = document.querySelectorAll("#oca-months li");
             if (months.length) {
                 months.forEach(function (month) {
                     month.addEventListener("click", function () {
                         OcaEvents.filterForMonth(month);
                     });
                 });
             }
 
             // Listener for 'today' button
             if (elsGotoToday.length) {
                 [].forEach.call(elsGotoToday, function (el) {
                     el.addEventListener("click", (e) => {
                         // if selected year = nowYear
                         let currentlySelectedYear;
                         let elCurrentlySelectedYear = document.querySelector(
                             "#oca-event-years li.current"
                         ); // should only be one
                         if (elCurrentlySelectedYear) {
                             currentlySelectedYear = parseInt(
                                 elCurrentlySelectedYear.dataset.year
                             );
                         } else {
                             currentlySelectedYear = null;
                         }
                         OcaEvents.resetSearch();
                         if (currentlySelectedYear !== parseInt(nowYear)) {
                             // select the current year in nav
                             let todayYearNav = document.querySelector(
                                 '#oca-event-years li[data-year="' +
                                     nowYear +
                                     '"] .select-event-year'
                             );
                             todayYearNav.click();
                             eventYearsMobile.value = nowYear;
                         }
                         OcaEvents.scrollToToday();
                     });
                 });
             }
 
 
             // Listener for event result docs show/hide clicker
             calendarRoot.addEventListener('click', (evt) => {
                const currentTarget = evt.target.closest('.oca-eventdocs--title');
                if ( currentTarget ) {
                    let title = currentTarget;
                    let doclist = currentTarget.nextElementSibling;
                    let chevron = title.firstElementChild;
                    if ( doclist.offsetParent === null ) {
                        chevron.classList.remove('fa-chevron-down');
                        chevron.classList.add('fa-chevron-up');
                        doclist.style.display = "block";
                    } else {
                        chevron.classList.remove('fa-chevron-up');
                        chevron.classList.add('fa-chevron-down');
                        doclist.style.display = "none";
                    }
                }
             });
 
         } else {
             console.log("Calendar root not found");
         }
     });
 
     let search = function () {
         pastEventsByYear = getPastEventsByYear();
         futureEventsByYear = getFutureEventsByYear();
         OcaEvents.doSearch(pastEventsByYear, futureEventsByYear);
     };
 
     let getState = function () {
         return state;
     };
 
     let getPastEventsByYear = function () {
         return pastEventsByYear;
     };
 
     let getFutureEventsByYear = function () {
         return futureEventsByYear;
     };
 
     let loaded = function () {
         state = getState();
         return state === "loaded";
     };
 
     return {
         state: getState,
         loaded: loaded,
         search: search,
         pastEvents: getPastEventsByYear,
         futureEvents: getFutureEventsByYear
     };
 }
 function getUniqueTagsOfType(pastEvents, futureEvents, tagType) {
    let uniqueTags = [];
    if (
        pastEvents.tags.hasOwnProperty(tagType) &&
        pastEvents.tags[tagType] !== null &&
        futureEvents.tags.hasOwnProperty(tagType) &&
        futureEvents.tags[tagType] !== null
    ) {
        let mergedTags = pastEvents.tags[tagType].concat(
            futureEvents.tags[tagType]
        );
        mergedTags.forEach((tag) => {
            if (!uniqueTags.includes(tag)) {
                uniqueTags.push(tag);
            }
        });
    }

    if ( pastEvents.tags.hasOwnProperty(tagType) && pastEvents.tags[tagType] !== null ) {
       uniqueTags = pastEvents.tags[tagType];
    } else if ( futureEvents.tags.hasOwnProperty(tagType) && futureEvents.tags[tagType] !== null) {
       uniqueTags = futureEvents.tags[tagType];
    }

    return uniqueTags;
 }
 
 function renderSkeleton(root, currentYear) {
     const html = `<div class="grid-container">
     <nav id="oca-events-nav" aria-label="Pagination">
         <div id="oca-search-holder" class="grid-x grid-margin-x">
             <div class="cell medium-6 text-center medium-text-right">
                 <button class="goToToday small hollow button"><i class="far fa-dot-circle"></i> TODAY</button>
             </div>
             <div class="cell medium-6 text-center medium-text-left">
             <form id="oca-event-search">
                 <div class="input-group">
                     <input name="oca-qs" id="oca-qs" class="input-group-field" type="text">
                     <div class="input-group-button">
                         <button type="submit" class="button"><i class="fas fa-search"></i></button>
                     </div>
                 </div>
             </form>
             </div> 
         </div>
         <select name="event-year" id="oca-event-years-mobile">
             <option value="${currentYear}" selected>${currentYear}</option>
         </select>
         <div id="oca-event-years-wrapper">
             <ul id="oca-event-years" class="pagination text-center">
                 <li data-year="${currentYear}" class="current"><span class="show-for-sr">You're at</span> ${currentYear}</li>
             </ul>
             <div class="paddles">
                 <button class="left-paddle paddle"><i class="fas fa-arrow-left"></i></button>
                 <button class="right-paddle paddle"><i class="fas fa-arrow-right"></i></button>
             </div>
         </div>
         <ul id="oca-months" class="pagination text-center">
             <li class="month" data-month="1"><button>Jan</button></li>
             <li class="month" data-month="2"><button>Feb</button></li>
             <li class="month" data-month="3"><button>Mar</button></li>
             <li class="month" data-month="4"><button>Apr</button></li>
             <li class="month" data-month="5"><button>May</button></li>
             <li class="month" data-month="6"><button>Jun</button></li>
             <li class="month" data-month="7"><button>Jul</button></li>
             <li class="month" data-month="8"><button>Aug</button></li>
             <li class="month" data-month="9"><button>Sep</button></li>
             <li class="month" data-month="10"><button>Oct</button></li>
             <li class="month" data-month="11"><button>Nov</button></li>
             <li class="month" data-month="12"><button>Dec</button></li>
             <li class="month-all"><button>All</button></li>
         </ul>
     </nav>
     <div id="oca-eventsdata">
         <div class="waiting text-center"><div class="fa-3x"><i class="fas fa-circle-notch fa-spin"></i></div></div>
     </div>
   </div>`;
     root.insertAdjacentHTML("afterbegin", html);
     console.info("OCA Calendar outline created");
 }
 